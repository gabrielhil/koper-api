﻿using Koper.Domain.Kernel;
using Koper.Infra.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Koper.Infra.Data.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly KoperContext _context;

        protected Repository(KoperContext context)
        {
            _context = context;
        }

        //

        public async Task<TEntity> GetById(Guid id)
        {
            return await _context.FindAsync<TEntity>(id);
        }

        public async Task Add(TEntity entity)
        {
            await _context.AddAsync(entity);
        }

        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            _context.Remove(entity);
        }
    }
}
