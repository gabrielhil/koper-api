﻿using Koper.Domain.Entities;
using Koper.Domain.Repositories;
using Koper.Infra.Data.Contexts;

namespace Koper.Infra.Data.Repositories
{
    public class BlocoRepository : Repository<Bloco>, IBlocoRepository
    {
        public BlocoRepository(KoperContext context) : base(context)
        {
        }
    }
}
