﻿using Koper.Domain.Entities;
using Koper.Domain.Repositories;
using Koper.Infra.Data.Contexts;

namespace Koper.Infra.Data.Repositories
{
    public class PavimentoRepository : Repository<Pavimento>, IPavimentoRepository
    {
        public PavimentoRepository(KoperContext context) : base(context)
        {
        }
    }
}
