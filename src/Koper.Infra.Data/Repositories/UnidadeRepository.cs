﻿using Koper.Domain.Entities;
using Koper.Domain.Repositories;
using Koper.Infra.Data.Contexts;

namespace Koper.Infra.Data.Repositories
{
    public class UnidadeRepository : Repository<Unidade>, IUnidadeRepository
    {
        public UnidadeRepository(KoperContext context) : base(context)
        {
        }
    }
}
