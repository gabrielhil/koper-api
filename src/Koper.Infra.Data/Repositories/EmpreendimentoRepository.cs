﻿using Koper.Domain.Entities;
using Koper.Domain.Repositories;
using Koper.Infra.Data.Contexts;

namespace Koper.Infra.Data.Repositories
{
    public class EmpreendimentoRepository : Repository<Empreendimento>, IEmpreendimentoRepository
    {
        public EmpreendimentoRepository(KoperContext context) : base(context)
        {
        }
    }
}
