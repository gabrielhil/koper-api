﻿using Koper.Domain.Entities;
using Koper.Domain.Repositories;
using Koper.Infra.Data.Contexts;

namespace Koper.Infra.Data.Repositories
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(KoperContext context) : base(context)
        {
        }
    }
}
