﻿using Koper.Domain.Repositories;
using Koper.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Koper.Infra.Data.Configuration
{
    public class RepositoriesDIConfiguration
    {
        public RepositoriesDIConfiguration(IServiceCollection services)
        {
            services.AddTransient<IBlocoRepository, BlocoRepository>();
            services.AddTransient<IClienteRepository, ClienteRepository>();
            services.AddTransient<IEmpreendimentoRepository, EmpreendimentoRepository>();
            services.AddTransient<IPavimentoRepository, PavimentoRepository>();
            services.AddTransient<IUnidadeRepository, UnidadeRepository>();
        }
    }
}
