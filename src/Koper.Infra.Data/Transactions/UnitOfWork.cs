﻿using Koper.Domain.Transactions;
using Koper.Infra.Data.Contexts;
using System.Threading.Tasks;

namespace Koper.Infra.Data.Transactions
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly KoperContext _context;

        public UnitOfWork(KoperContext context)
        {
            _context = context;
        }

        //

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        public Task Rollback()
        {
            // Do nothing :)
            return Task.CompletedTask;
        }
    }
}
