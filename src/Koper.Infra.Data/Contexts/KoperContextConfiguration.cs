﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Koper.Infra.Data.Contexts
{
    public class KoperContextConfiguration
    {
        public KoperContextConfiguration(IServiceCollection services, string connectionString)
        {
            services.AddDbContext<KoperContext>(o => o.UseSqlServer(connectionString));
        }
    }
}
