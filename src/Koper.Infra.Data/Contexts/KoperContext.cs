﻿using FluentValidator;
using Koper.Domain.Entities;
using Koper.Infra.Data.Maps;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Contexts
{
    public class KoperContext : DbContext
    {
        public KoperContext(DbContextOptions<KoperContext> options) : base(options) { }

        //

        public DbSet<Bloco> Blocos { get; set; }

        public DbSet<Cliente> Clientes { get; set; }

        public DbSet<Empreendimento> Empreendimentos { get; set; }

        public DbSet<Pavimento> Pavimentos { get; set; }

        public DbSet<Unidade> Unidades { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Ignore<Notification>();
            
            new BlocoMap().Configure(modelBuilder);
            new ClienteMap().Configure(modelBuilder);
            new EmpreendimentoMap().Configure(modelBuilder);
            new PavimentoMap().Configure(modelBuilder);
            new UnidadeMap().Configure(modelBuilder);
        }
    }
}
