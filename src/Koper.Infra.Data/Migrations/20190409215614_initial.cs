﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Koper.Infra.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Clientes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    CPF = table.Column<string>(type: "varchar(11)", nullable: true),
                    DataNascimento = table.Column<DateTime>(nullable: true),
                    Endereco = table.Column<string>(type: "varchar(200)", nullable: true),
                    Renda = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Empreendimentos",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    DataInicio = table.Column<DateTime>(nullable: true),
                    DataConclusao = table.Column<DateTime>(nullable: true),
                    TipoEmpreendimento = table.Column<int>(nullable: false),
                    Endereco = table.Column<string>(type: "varchar(200)", nullable: true),
                    AreaTotal = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empreendimentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Blocos",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    EmpreendimentoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blocos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blocos_Empreendimentos_EmpreendimentoId",
                        column: x => x.EmpreendimentoId,
                        principalSchema: "dbo",
                        principalTable: "Empreendimentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pavimentos",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Posicao = table.Column<int>(nullable: false),
                    BlocoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pavimentos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pavimentos_Blocos_BlocoId",
                        column: x => x.BlocoId,
                        principalSchema: "dbo",
                        principalTable: "Blocos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Unidades",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(200)", nullable: false),
                    Status = table.Column<int>(nullable: false),
                    AreaComum = table.Column<double>(nullable: false),
                    Vendavel = table.Column<bool>(nullable: false),
                    Valor = table.Column<double>(nullable: false),
                    ValorMinimoDeVenda = table.Column<double>(nullable: false),
                    PavimentoId = table.Column<Guid>(nullable: true),
                    BlocoId = table.Column<Guid>(nullable: true),
                    EmpreendimentoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unidades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Unidades_Blocos_BlocoId",
                        column: x => x.BlocoId,
                        principalSchema: "dbo",
                        principalTable: "Blocos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Unidades_Empreendimentos_EmpreendimentoId",
                        column: x => x.EmpreendimentoId,
                        principalSchema: "dbo",
                        principalTable: "Empreendimentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Unidades_Pavimentos_PavimentoId",
                        column: x => x.PavimentoId,
                        principalSchema: "dbo",
                        principalTable: "Pavimentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Blocos_EmpreendimentoId",
                schema: "dbo",
                table: "Blocos",
                column: "EmpreendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Pavimentos_BlocoId",
                schema: "dbo",
                table: "Pavimentos",
                column: "BlocoId");

            migrationBuilder.CreateIndex(
                name: "IX_Unidades_BlocoId",
                schema: "dbo",
                table: "Unidades",
                column: "BlocoId");

            migrationBuilder.CreateIndex(
                name: "IX_Unidades_EmpreendimentoId",
                schema: "dbo",
                table: "Unidades",
                column: "EmpreendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Unidades_PavimentoId",
                schema: "dbo",
                table: "Unidades",
                column: "PavimentoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clientes",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Unidades",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Pavimentos",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Blocos",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Empreendimentos",
                schema: "dbo");
        }
    }
}
