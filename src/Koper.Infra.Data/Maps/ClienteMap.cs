﻿using Koper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Maps
{
    internal class ClienteMap
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            // Table

            modelBuilder.Entity<Cliente>()
                .ToTable("Clientes", "dbo")
                .HasKey(x => x.Id);

            // Properties

            modelBuilder.Entity<Cliente>()
                .Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(x => x.CPF)
               .HasColumnType("varchar(11)");

            modelBuilder.Entity<Cliente>()
               .Property(x => x.Endereco)
               .HasColumnType("varchar(200)");
        }
    }
}
