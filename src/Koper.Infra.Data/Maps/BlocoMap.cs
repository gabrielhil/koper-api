﻿using Koper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Maps
{
    internal class BlocoMap
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            // Table

            modelBuilder.Entity<Bloco>()
                .ToTable("Blocos", "dbo")
                .HasKey(x => x.Id);

            // Properties

            modelBuilder.Entity<Bloco>()
                .Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();
        }
    }
}
