﻿using Koper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Maps
{
    internal class PavimentoMap
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            // Table

            modelBuilder.Entity<Pavimento>()
                .ToTable("Pavimentos", "dbo")
                .HasKey(x => x.Id);

            // Properties

            modelBuilder.Entity<Pavimento>()
                .Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();
        }
    }
}
