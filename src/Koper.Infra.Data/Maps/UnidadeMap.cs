﻿using Koper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Maps
{
    internal class UnidadeMap
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            // Table

            modelBuilder.Entity<Unidade>()
                .ToTable("Unidades", "dbo")
                .HasKey(x => x.Id);

            // Properties

            modelBuilder.Entity<Unidade>()
                .Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();
        }
    }
}
