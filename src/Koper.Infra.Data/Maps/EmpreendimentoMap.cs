﻿using Koper.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Koper.Infra.Data.Maps
{
    internal class EmpreendimentoMap
    {
        public void Configure(ModelBuilder modelBuilder)
        {
            // Table

            modelBuilder.Entity<Empreendimento>()
                .ToTable("Empreendimentos", "dbo")
                .HasKey(x => x.Id);

            // Properties

            modelBuilder.Entity<Empreendimento>()
                .Property(x => x.Nome)
                .HasColumnType("varchar(200)")
                .IsRequired();

            modelBuilder.Entity<Empreendimento>()
               .Property(x => x.Endereco)
               .HasColumnType("varchar(200)");
        }
    }
}
