﻿using Koper.Application.Configuration;
using Koper.Domain.Transactions;
using Koper.Infra.Data.Configuration;
using Koper.Infra.Data.Contexts;
using Koper.Infra.Data.Transactions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Koper.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var configurationBuilder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddEnvironmentVariables();

            Configuration = configurationBuilder.Build();
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // Connection String
            string KoperConnectionString = Configuration.GetConnectionString("KoperConnectionString");

            // Infra - Repositories Dependency Injection Configuration
            _ = new RepositoriesDIConfiguration(services);

            // Application - Dependency Injection Configuration
            _ = new ApplicationDIConfiguration(services);

            // KoperContext Configuration
            _ = new KoperContextConfiguration(services, KoperConnectionString);

            // Unit of Work Configuration
            services.AddTransient<IUnitOfWork, UnitOfWork>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
