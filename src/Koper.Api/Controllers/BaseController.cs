﻿using FluentValidator;
using Koper.Domain.Transactions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Koper.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BaseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //

        public new async Task<IActionResult> Response(object result, IEnumerable<Notification> notifications)
        {
            if (!notifications.Any())
            {
                try
                {
                    await _unitOfWork.Commit();
                    return Ok(new
                    {
                        success = true,
                        data = result
                    });
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();

                    Console.WriteLine(ex.Message);

                    return BadRequest(new
                    {
                        success = false,
                        errors = new[] { "Ocorreu uma falha interna no servidor." }
                    });
                }
            }
            else
            {
                await _unitOfWork.Rollback();

                return BadRequest(new
                {
                    success = false,
                    errors = notifications
                });
            }
        }

        public new async Task<IActionResult> Response(object result)
        {
            try
            {
                await _unitOfWork.Commit();
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }
            catch (Exception ex)
            {
                await _unitOfWork.Rollback();

                Console.WriteLine(ex.Message);

                return BadRequest(new
                {
                    success = false,
                    errors = new[] { "Ocorreu uma falha interna no servidor." }
                });
            }
        }
    }
}
