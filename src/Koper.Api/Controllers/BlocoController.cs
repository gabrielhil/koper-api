﻿using Koper.Application.Handlers;
using Koper.Application.Inputs;
using Koper.Domain.Transactions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Koper.Api.Controllers
{
    public class BlocoController : BaseController
    {
        private readonly CadastrarBlocoCommandHandler _cadastrarBlocoCommandHandler;

        public BlocoController(CadastrarBlocoCommandHandler cadastrarBlocoCommandHandler,
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _cadastrarBlocoCommandHandler = cadastrarBlocoCommandHandler;
        }

        //

        [HttpPost]
        [Route("v1/bloco")]
        public async Task<IActionResult> Cadastrar([FromBody] CadastrarBlocoCommandInput command)
        {
            var result = _cadastrarBlocoCommandHandler.Handle(command);
            return await Response(result, _cadastrarBlocoCommandHandler.Notifications);
        }
    }
}
