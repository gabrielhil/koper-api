﻿namespace Koper.Domain.Enums
{
    public enum TipoEmpreendimento
    {
        Comercial = 1,
        Residencial = 2,
        Comercial_e_Residencial = 3,
        Loteamento = 4
    }
}
