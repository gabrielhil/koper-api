﻿namespace Koper.Domain.Enums
{
    public enum StatusUnidade
    {
        Disponivel = 1,
        Indisponivel = 2,
        Reservada = 3,
        Vendida = 4
    }
}
