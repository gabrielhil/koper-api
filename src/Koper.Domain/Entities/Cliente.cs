﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Domain.Entities
{
    public class Cliente : Entity
    {
        // EF Core
        protected Cliente() { }

        //

        public string Nome { get; set; }

        public string CPF { get; set; }

        public DateTime? DataNascimento { get; set; }

        public string Endereco { get; set; }

        public double Renda { get; set; }
    }
}
