﻿using Koper.Domain.Enums;
using Koper.Domain.Kernel;

namespace Koper.Domain.Entities
{
    public class Unidade : Entity
    {
        // EF Core
        protected Unidade() { }

        //

        public string Nome { get; set; }

        public StatusUnidade Status { get; set; }

        public double AreaComum { get; set; }

        public bool Vendavel { get; set; }

        public double Valor { get; set; }

        public double ValorMinimoDeVenda { get; set; }

        //

        public Pavimento Pavimento { get; set; }

        public Bloco Bloco { get; set; }

        public Empreendimento Empreendimento { get; set; }
    }
}
