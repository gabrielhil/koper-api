﻿using Koper.Domain.Kernel;
using System.Collections.Generic;

namespace Koper.Domain.Entities
{
    public class Bloco : Entity
    {
        // EF Core
        protected Bloco() { }

        public Bloco(string nome)
        {
            Nome = nome;
            Pavimentos = new List<Pavimento>();
            Unidades = new List<Unidade>();
        }

        public Bloco(string nome, Empreendimento empreendimento)
        {
            Nome = nome;
            Empreendimento = empreendimento;
            Pavimentos = new List<Pavimento>();
            Unidades = new List<Unidade>();
        }

        //

        public string Nome { get; set; }


        public Empreendimento Empreendimento { get; set; }

        //

        public List<Pavimento> Pavimentos { get; set; }

        public List<Unidade> Unidades { get; set; }
    }
}
