﻿using Koper.Domain.Enums;
using Koper.Domain.Kernel;
using System;
using System.Collections.Generic;

namespace Koper.Domain.Entities
{
    public class Empreendimento : Entity
    {
        public Empreendimento()
        {
            Blocos = new List<Bloco>();
            Unidades = new List<Unidade>();
        }

        //

        public string Nome { get; set; }

        public DateTime? DataInicio { get; set; }

        public DateTime? DataConclusao { get; set; }

        public TipoEmpreendimento TipoEmpreendimento { get; set; }

        public string Endereco { get; set; }

        public double AreaTotal { get; set; }

        public List<Bloco> Blocos { get; set; }

        public List<Unidade> Unidades { get; set; }
    }
}
