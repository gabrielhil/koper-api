﻿using Koper.Domain.Kernel;
using System;
using System.Collections.Generic;

namespace Koper.Domain.Entities
{
    public class Pavimento : Entity
    {
        public Pavimento()
        {
            Unidades = new List<Unidade>();
        }

        //

        public string Nome { get; set; }

        public int Posicao { get; set; }

        public List<Unidade> Unidades { get; set; }
    }
}
