﻿using Koper.Domain.Entities;
using Koper.Domain.Kernel;

namespace Koper.Domain.Repositories
{
    public interface IClienteRepository : IRepository<Cliente>
    { }
}
