﻿using FluentValidator;
using System;

namespace Koper.Domain.Kernel
{
    public class Entity : Notifiable
    {
        public Entity()
        {
            Id = Guid.NewGuid();
        }

        //

        public Guid Id { get; private set; }
    }
}
