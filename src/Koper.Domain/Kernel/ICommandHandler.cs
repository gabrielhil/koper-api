﻿using System.Threading.Tasks;

namespace Koper.Domain.Kernel
{
    public interface ICommandHandler<I, R> where I : ICommandInput where R : ICommandResult
    {
        Task<R> Handle(I command);
    }
}
