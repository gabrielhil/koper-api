﻿using FluentValidator;

namespace Koper.Domain.Kernel
{
    public interface ICommandInput
    {
        bool IsValid { get; }

        bool Validate();
    }
}
