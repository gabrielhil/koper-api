﻿using System.Threading.Tasks;

namespace Koper.Domain.Transactions
{
    public interface IUnitOfWork
    {
        Task Commit();

        Task Rollback();
    }
}
