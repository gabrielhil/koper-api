﻿using Koper.Domain.Enums;
using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class EditarUnidadeCommandInput : ICommandInput
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }

        public StatusUnidade Status { get; set; }

        public double AreaComum { get; set; }

        public bool Vendavel { get; set; }

        public double Valor { get; set; }

        public double ValorMinimoDeVenda { get; set; }

        //

        public Guid? PavimentoId { get; set; }

        public Guid? BlocoId { get; set; }

        public Guid? EmpreendimentoId { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
