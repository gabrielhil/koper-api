﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class ExcluirUnidadeCommandInput : ICommandInput
    {
        public Guid Id { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
