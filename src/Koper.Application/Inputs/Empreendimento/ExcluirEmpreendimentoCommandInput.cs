﻿using Koper.Domain.Kernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Koper.Application.Inputs
{
    public class ExcluirEmpreendimentoCommandInput : ICommandInput
    {
        public Guid Id { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
