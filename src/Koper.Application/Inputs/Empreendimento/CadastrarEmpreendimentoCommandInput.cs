﻿using Koper.Domain.Enums;
using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class CadastrarEmpreendimentoCommandInput : ICommandInput
    {
        public string Nome { get; set; }

        public DateTime? DataInicio { get; set; }

        public DateTime? DataConclusao { get; set; }

        public TipoEmpreendimento TipoEmpreendimento { get; set; }

        public string Endereco { get; set; }

        public double AreaTotal { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
