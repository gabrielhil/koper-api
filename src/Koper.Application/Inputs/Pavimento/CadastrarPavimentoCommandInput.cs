﻿using Koper.Domain.Kernel;

namespace Koper.Application.Inputs
{
    public class CadastrarPavimentoCommandInput : ICommandInput
    {
        public string Nome { get; set; }

        public int Posicao { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
