﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class EditarPavimentoCommandInput : ICommandInput
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }

        public int Posicao { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
