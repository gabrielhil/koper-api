﻿using FluentValidator;
using FluentValidator.Validation;
using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class CadastrarBlocoCommandInput : Notifiable, ICommandInput
    {
        public string Nome { get; set; }

        public Guid? EmpreendimentoId { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            IsValid = true;

            new ValidationContract()
                .IsNullOrEmpty(Nome, "Nome", "O nome é obrigatório");

            AddNotifications(Notifications);

            return IsValid;
        }
    }
}
