﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Inputs
{
    public class EditarClienteCommandInput : ICommandInput
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascimento { get; set; }

        public string Endereco { get; set; }

        public double Renda { get; set; }

        //

        public bool IsValid { get; private set; }

        public bool Validate()
        {
            return IsValid = true;
        }
    }
}
