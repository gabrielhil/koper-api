﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Results
{
    public class CadastrarEmpreendimentoCommandResult : ICommandResult
    {
        public CadastrarEmpreendimentoCommandResult(Guid id)
        {
            Id = id;
        }

        //

        public Guid Id { get; }
    }
}
