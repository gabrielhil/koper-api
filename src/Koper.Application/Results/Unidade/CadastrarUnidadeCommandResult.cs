﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Results
{
    public class CadastrarUnidadeCommandResult : ICommandResult
    {
        public CadastrarUnidadeCommandResult(Guid id)
        {
            Id = id;
        }

        //

        public Guid Id { get; }
    }
}
