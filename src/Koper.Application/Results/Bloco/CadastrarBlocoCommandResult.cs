﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Results
{
    public class CadastrarBlocoCommandResult : ICommandResult
    {
        public CadastrarBlocoCommandResult(Guid id)
        {
            Id = id;
        }

        //

        public Guid Id { get; }
    }
}
