﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Results
{
    public class CadastrarPavimentoCommandResult : ICommandResult
    {
        public CadastrarPavimentoCommandResult(Guid id)
        {
            Id = id;
        }

        //

        public Guid Id { get; }
    }
}
