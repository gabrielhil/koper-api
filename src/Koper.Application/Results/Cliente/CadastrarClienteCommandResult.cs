﻿using Koper.Domain.Kernel;
using System;

namespace Koper.Application.Results
{
    public class CadastrarClienteCommandResult : ICommandResult
    {
        public CadastrarClienteCommandResult(Guid id)
        {
            Id = id;
        }

        //

        public Guid Id { get; }
    }
}
