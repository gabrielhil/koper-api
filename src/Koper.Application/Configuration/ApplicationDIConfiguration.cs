﻿using Koper.Application.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Koper.Application.Configuration
{
    public class ApplicationDIConfiguration
    {
        public ApplicationDIConfiguration(IServiceCollection services)
        {
            // Blocos
            services.AddTransient<CadastrarBlocoCommandHandler, CadastrarBlocoCommandHandler>();
        }
    }
}
