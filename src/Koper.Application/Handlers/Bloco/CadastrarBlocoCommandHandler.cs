﻿using FluentValidator;
using Koper.Application.Inputs;
using Koper.Application.Results;
using Koper.Domain.Entities;
using Koper.Domain.Kernel;
using Koper.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace Koper.Application.Handlers
{
    public class CadastrarBlocoCommandHandler : Notifiable, ICommandHandler<CadastrarBlocoCommandInput, CadastrarBlocoCommandResult>
    {
        private readonly IEmpreendimentoRepository _empreendimentoRepository;
        private readonly IBlocoRepository _blocoRepository;

        public CadastrarBlocoCommandHandler(
            IEmpreendimentoRepository empreendimentoRepository,
            IBlocoRepository blocoRepository)
        {
            _empreendimentoRepository = empreendimentoRepository;
            _blocoRepository = blocoRepository;
        }

        //

        public async Task<CadastrarBlocoCommandResult> Handle(CadastrarBlocoCommandInput command)
        {
            if (!command.Validate())
                AddNotifications(command.Notifications);

            dynamic bloco;

            if (command.EmpreendimentoId.HasValue)
            {
                var empreendimento = await _empreendimentoRepository.GetById((Guid)command.EmpreendimentoId);
                bloco = new Bloco(command.Nome, empreendimento);
            }
            else
            {
                bloco = new Bloco(command.Nome);
            }

            // Domain Notifications
            AddNotifications(bloco.Notifications);

            _blocoRepository.Add(bloco);

            return new CadastrarBlocoCommandResult(bloco.Id);
        }
    }
}
